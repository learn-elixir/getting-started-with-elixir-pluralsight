defmodule PluralsightTweetTest do
  use ExUnit.Case
  doctest PluralsightTweet

  test "the truth" do
    assert 1 + 1 == 2
  end

  @tag watching: true
  test "another test" do
    # failing assertion
    # assert 2 + 2 == 5
    # passing assertion
    assert 2 + 2 == 4
  end
end
