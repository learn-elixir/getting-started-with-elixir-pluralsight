defmodule Sample.Enum do
    def first(list) do
        if (length(list) == 0) do
            nil
        else
            hd(list)
        end
    end

    def map([], _), do: []
    def map([hd | tl], f) do
        [f.(hd) | map(tl, f)]
    end

    def length([]), do: 0
    def length([_ | tail]),
        do: 1 + length(tail)

    # tail recursion example
    def other_length([_ | tail]),
        do: other_length(tail, 1)
    def other_length([], len),
        do: len
    def other_length([_ | tail], len),
        do: other_length(tail, len + 1)
end