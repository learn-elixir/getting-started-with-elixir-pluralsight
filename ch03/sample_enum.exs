defmodule Sample.Enum do
# 1. version
#    def first(list) do
#        hd(list)
#    end

# 2. version
#    def first([]) do
#        nil
#    end
#
#    def first([head | _]) do
#        head
#    end

# 3. version
#    def first([]), do: nil

# 4. version
#    def first(list) when length(list) == 0, do: nil

# 5. version, NOTE: have to swap order
#    def first([head | _]), do: head
#    def first([], val \\ nil), do: val

# 6. version
    def first(list, val \\ nil)
    def first([head | _], _), do: head
    def first([], val), do: val

    def add(list, val \\ 0) do
        trace(val)
        [val | list]
    end

    defp trace(string) do
        IO.puts("The value passed in was #{string}")
    end
end